import torch
from torch.utils.data import Dataset
import pandas as pd

"""
TOTO: 
1- Add target variable
2- Split dataset to train, test, dev sets.
3- find the mean and std of the target varialbes in training dataset
"""


def standardize_dataframe(df_train, df_test):

    for c in df_train.columns:
        mean = df_train[c].mean()
        stdev = df_train[c].std()

        df_train[c] = (df_train[c] - mean) / stdev
        df_test[c] = (df_test[c] - mean) / stdev

    return df_train, df_test


def shift_dataframe(df, columns_to_shift, shift_val=1):
    df[columns_to_shift] = df[columns_to_shift].shift(-shift_val)
    df = df.iloc[:-shift_val]
    return df


class SequenceDataset(Dataset):
    def __init__(self, dataframe, target, features, sequence_length=5):
        self.features = features
        self.target = target
        self.sequence_length = sequence_length
        self.y = torch.tensor(dataframe[target].values).float()
        self.X = torch.tensor(dataframe[features].values).float()

    def __len__(self):
        return self.X.shape[0]

    def __getitem__(self, i):
        if i >= self.sequence_length - 1:
            i_start = i - self.sequence_length + 1
            x = self.X[i_start:(i + 1), :]
        else:
            padding = self.X[0].repeat(self.sequence_length - i - 1, 1)
            x = self.X[0:(i + 1), :]
            x = torch.cat((padding, x), 0)

        return x, self.y[i]
